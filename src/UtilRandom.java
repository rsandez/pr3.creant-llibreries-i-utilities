import java.util.Random;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class UtilRandom {
    private static final Random rand = new Random();

    public static int generarNumero() {
        return rand.nextInt();
    }

    public static String generarColor() {
        int r = rand.nextInt(256);
        int g = rand.nextInt(256);
        int b = rand.nextInt(256);
        return String.format("#%02x%02x%02x", r, g, b);
    }

    public static boolean generarBoolean(double probabilitatCertesa) {
        return rand.nextDouble() < probabilitatCertesa;
    }

    public static String generarTelefon() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 9; i++) {
            sb.append(rand.nextInt(10));
        }
        return sb.toString();
    }

    public static LocalDate generarData() {
        long minDay = LocalDate.of(1900, 1, 1).toEpochDay();
        long maxDay = LocalDate.now().toEpochDay();
        long randomDay = minDay + rand.nextLong() % (maxDay - minDay);
        return LocalDate.ofEpochDay(randomDay);
    }

    public static String generarUsuari() {
        String[] prefixos = {"user", "admin", "guest", "test"};
        String[] sufixos = {"123", "456", "789", "abc"};
        int prefixIndex = rand.nextInt(prefixos.length);
        int sufixIndex = rand.nextInt(sufixos.length);
        return prefixos[prefixIndex] + sufixos[sufixIndex];
    }
}
