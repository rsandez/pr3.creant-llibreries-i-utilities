
public class Enum {


	/**
	 * Enum que defineix els tipus de format de fitxer suportats.
	 */
	public enum TipusFormatFitxer {
		TXT(".txt"),
		PDF(".pdf"),
		DOC(".doc"),
		XLS(".xls"),
		CSV(".csv");

		private final String extensio;

		private TipusFormatFitxer(String extensio) {
			this.extensio = extensio;
		}

		/**
		 * Obté l'extensió associada a aquest tipus de format.
		 *
		 * @return l'extensió del fitxer
		 */
		public String getExtensio() {
			return extensio;
		}
	}
	/**
	 * Enum que defineix els tipus de producte que es poden vendre.
	 */
	public enum TipusProducte {
		ALIMENTARI("Alimentari"),
		ELECTRÒNIC("Electrònic"),
		ROBA("Roba"),
		LLIBRE("Llibre");

		private final String nom;

		private TipusProducte(String nom) {
			this.nom = nom;
		}

		/**
		 * Obté el nom d'aquest tipus de producte.
		 *
		 * @return el nom del producte
		 */
		public String getNom() {
			return nom;
		}
	}
	/**
	 * Enum que defineix les direccions cardinals.
	 */
	public enum DireccioCardinal {
		NORD("Nord", 0),
		EST("Est", 90),
		SUD("Sud", 180),
		OEST("Oest", 270);

		private final String nom;
		private final int graus;

		private DireccioCardinal(String nom, int graus) {
			this.nom = nom;
			this.graus = graus;
		}

		/**
		 * Obté el nom d'aquesta direcció cardinal.
		 *
		 * @return el nom de la direcció cardinal
		 */
		public String getNom() {
			return nom;
		}

		/**
		 * Obté l'angle en graus corresponent a aquesta direcció cardinal.
		 *
		 * @return l'angle en graus
		 */
		public int getGraus() {
			return graus;
		}
	}

}
