
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class UtilConsole {

	static Scanner teclado = new Scanner(System.in);

	public static int pedirEntero(String etiqueta) {


		int entero = 0;
		boolean validarEntero = false;

		do {
			System.out.println(etiqueta);
			if (teclado.hasNextInt()) {
				entero = Integer.parseInt(teclado.nextLine());
				validarEntero = true;
			} else {
				teclado.nextLine();
			}
		} while (!validarEntero);

		return entero;
	}
	
	public static String pedirString (String etiqueta) {
		String texto = "";
		boolean validarString = false;

		do {
			System.out.println(etiqueta);
			if (teclado.hasNext()) {
				texto = teclado.nextLine();
				validarString = true;

			} else {
				teclado.next();
			}
		} while (!validarString);

		return texto;
	}
	
	public static String pedirDNI (String etiqueta) {
		String dni;
		boolean dniCorrecto = false;

		do {
			dni = pedirString(etiqueta);
			dniCorrecto = validarDNI(dni);
		} while (!dniCorrecto);

		return dni;
	}
	
	public static boolean validarDNI (String dni) {

		String letras = "TRWAGMYFPDXBNJZSQVHLCKE";

		if (dni.matches("[0-9]{7,8}[A-Z a-z]")) {
			int numDNI = Integer.parseInt(dni.substring(0, dni.length()-1));
			char letraDNI = dni.charAt(dni.length()-1);
			int mod = numDNI % 23;
			if (letras.charAt(mod) == letraDNI) return true;
		}
		return false;
	}
	
	public static LocalDate pedirDataneixement( String etiqueta) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

		String dataString = "";
		LocalDate data = null;
		boolean isDate = false;

		do {
			dataString = pedirString(etiqueta);
			try {
				data = LocalDate.parse(dataString, dtf);
				isDate = true;
			} catch (DateTimeParseException e) {
				System.out.println("Fecha incorrecta");
			}
		} while (!isDate);
		return data;
	}
	
	public static String pedirCP (String etiqueta) {
		String cp = "";

		do {
			System.out.print("CP:");
			cp = teclado.nextLine();

		} while (!cp.matches("[0-9]{5}"));
		return cp;
	}
	
	public static String pedirTelefono(String etiqueta) {
		String telefono;

		do {
			System.out.print("Teléfono:");
			telefono = teclado.nextLine();	
		} while (!telefono.matches("[6-7][0-9]{8}")); 

		return telefono;
	}
	
	public static double pedirDouble(String etiqueta) {
		double decimal = 0;
		boolean validarDouble = false;

		do {

			System.out.println(etiqueta);
			decimal = teclado.nextDouble();

			if (Double.isNaN(decimal)) {
				validarDouble = false;

			} else {
				validarDouble = true;
				teclado.nextLine();

			}
		} while (!validarDouble);

		return decimal;
	}

}